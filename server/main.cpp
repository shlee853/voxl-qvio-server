/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <getopt.h>
#include <sched.h>

#include <mvVISLAM.h>

#include "yaml-cpp/yaml.h"
#include <iostream>
#include <fstream>

#include <c_library_v2/common/mavlink.h> // include before modal_pipe!
#include <modal_pipe.h>
#include <voxl_common_config.h>

#include <voxl_qvio_server.h>
#include "config_file.h"
#include "cCharacter.h"

// server channels
#define EXTENDED_CH	0
#define SIMPLE_CH	1
#define OVERLAY_CH	2

// client channels and config
#define IMU_CH	0
#define CAM_CH	1
#define GPS_CH	2
#define IMU_PIPE_MIN_PIPE_SIZE (1  * 1024 * 1024) // give ourselves huge buffers
#define CAM_PIPE_SIZE (64 * 1024 * 1024) // give ourselves huge buffers
#define PROCESS_NAME	"qvio-server"

#define GPS_PIPE_NAME	"vvpx4_vehicle_gps_position"


static int en_config_only = 0;
static int en_debug = 0;
static int en_debug_pos = 0;
static int en_debug_timing = 0;
static int en_debug_overlay = 0;
static pthread_t data_thread;

// these are the last timestamps that have completely passed into mvvislam
static volatile int64_t last_imu_timestamp_ns = 0;
static volatile int64_t last_cam_timestamp_ns = 0;

// state of imu and camera connections
static volatile int is_imu_connected = 0;
static volatile int is_cam_connected = 0;


static mvVISLAM* mv_vislam_ptr;
static int blank_counter = 0;
static int64_t last_time_alignment_ns = 0;

// mutex to protect the cam frame processing during reset
static pthread_mutex_t cam_mtx = PTHREAD_MUTEX_INITIALIZER;

#define DRAW_BONUS_ROWS_TOP 32
#define DRAW_BONUS_ROWS_BOT 32
static pthread_mutex_t          draw_mtx     = PTHREAD_MUTEX_INITIALIZER;
static bool                     draw_ready   = false;
static uint8_t*                 draw_frame   = NULL;
static camera_image_metadata_t  draw_meta;


// set any error codes here for publishing in the data structure in addition
// to errors that come from mvVISLAM
static uint32_t global_error_codes = 0;

// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-c, --config                only parse the config file and exit, don't run\n\
-d, --debug                 enable debug prints\n\
-h, --help                  print this help message\n\
-o, --debug-overlay         show all points on the overlay, not just in-state ones\n\
-p, --position              print position and rotation\n\
-t, --timing                enable timing debug prints\n\
\n");
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"config",			no_argument,		0, 'c'},
		{"debug",			no_argument,		0, 'd'},
		{"help",			no_argument,		0, 'h'},
		{"debug-overlay",	no_argument,		0, 'o'},
		{"position",		no_argument,		0, 'p'},
		{"timing",			no_argument,		0, 't'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "cdhopt", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			en_config_only = 1;
			break;

		case 'd':
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 'o':
			en_debug_overlay = 1;
			break;

		case 'p':
			en_debug_pos = 1;
			break;

		case 't':
			en_debug_timing = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


static int64_t _apps_time_monotonic_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_MONOTONIC, &ts)){
		fprintf(stderr,"ERROR calling clock_gettime\n");
		return -1;
	}
	return (int64_t)ts.tv_sec*1000000000 + (int64_t)ts.tv_nsec;
}


static void _nanosleep(uint64_t ns){
	struct timespec req,rem;
	req.tv_sec = ns/1000000000;
	req.tv_nsec = ns%1000000000;
	// loop untill nanosleep sets an error or finishes successfully
	errno=0; // reset errno to avoid false detection
	while(nanosleep(&req, &rem) && errno==EINTR){
		req.tv_sec = rem.tv_sec;
		req.tv_nsec = rem.tv_nsec;
	}
	return;
}


static float _qvio_quality(struct mvVISLAMPose pose)
{
	// vio quality uses covariance and a reset counter
	// the whole covarience matrix should be zero in case of blowup
	if(pose.errCovPose[3][3]<=0.0f || pose.errCovPose[4][4]<=0.0f || pose.errCovPose[5][5]<=0.0f){
		return -1.0f;
	}

	// pick the maximum (worst) of the position diagonal entries
	float max = pose.errCovPose[3][3];
	if(pose.errCovPose[4][4]>max) max = pose.errCovPose[4][4];
	if(pose.errCovPose[5][5]>max) max = pose.errCovPose[5][5];

	// covarience is very small. Scale it by 10^5 to make the output more readable
	float conf = 0.00001f / max;

	// return normal value
	return conf;
}


// convert roll/pitch/yaw in degrees from extrinsics config file to axis-angle
static int _tait_bryan_xyz_intrinsic_to_axis_angle(double tb_deg[3], float aa[3])
{
	if(tb_deg==NULL||aa==NULL){
		fprintf(stderr,"ERROR: in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}

	// convert from degree to rad
	double tb_rad[3];
	tb_rad[0] = tb_deg[0]*M_PI/180.0;
	tb_rad[1] = tb_deg[1]*M_PI/180.0;
	tb_rad[2] = tb_deg[2]*M_PI/180.0;

	double c1 = cos(tb_rad[0]/2.0);
	double s1 = sin(tb_rad[0]/2.0);
	double c2 = cos(tb_rad[1]/2.0);
	double s2 = sin(tb_rad[1]/2.0);
	double c3 = cos(tb_rad[2]/2.0);
	double s3 = sin(tb_rad[2]/2.0);
	double c1c2 = c1*c2;
	double s1s2 = s1*s2;
	double s1c2 = s1*c2;
	double c1s2 = c1*s2;

	// XYZ
	double w = c1c2*c3 - s1s2*s3;
	double x = s1c2*c3 + c1s2*s3;
	double y = c1s2*c3 - s1c2*s3;
	double z = c1c2*s3 + s1s2*c3;

	double norm = x*x+y*y+z*z;
	if(norm < 0.0001){
		aa[0]=0.0f;
		aa[1]=0.0f;
		aa[2]=0.0f;
		return 0;
	}

	double angle = 2.0*acos(w);
	double scale = angle/sqrt(norm);
	aa[0] = x*scale;
	aa[1] = y*scale;
	aa[2] = z*scale;
	return 0;
}

#define CONTROL_COMMANDS (RESET_VIO_SOFT "," RESET_VIO_HARD "," BLANK_VIO_CAM_COMMAND)
// control listens for reset commands
static void _control_pipe_cb(__attribute__((unused)) int ch, char* string, \
							 int bytes, __attribute__((unused)) void* context)
{
	// remove the trailing newline from echo
	if(bytes>1 && string[bytes-1]=='\n'){
		string[bytes-1]=0;
	}

	if(strcmp(string, RESET_VIO_SOFT)==0){
		printf("Client requested soft reset\n");
		pthread_mutex_lock(&cam_mtx);
		mvVISLAM_Reset(mv_vislam_ptr, 0);
		pthread_mutex_unlock(&cam_mtx);
		return;
	}
	if(strcmp(string, RESET_VIO_HARD)==0){
		printf("Client requested hard reset\n");
		pthread_mutex_lock(&cam_mtx);
		mvVISLAM_Reset(mv_vislam_ptr, 1);
		pthread_mutex_unlock(&cam_mtx);
		return;
	}
	if(strcmp(string, BLANK_VIO_CAM_COMMAND)==0){
		printf("Client requested image blank test\n");
		blank_counter = 30;
		return;
	}

	printf("WARNING: Server received unknown command through the control pipe!\n");
	printf("got %d bytes. Command is: %s\n", bytes, string);
	return;
}


// print when a new client connects to us
static void _overlay_connect_cb(	__attribute__((unused))int ch, \
							__attribute__((unused))int client_id,
							char* client_name,\
							__attribute__((unused)) void* context)
{
	printf("client \"%s\" connected to overlay\n", client_name);
	return;
}


// print when a client disconnects from us
static void _overlay_disconnect_cb(	__attribute__((unused))int ch, \
							__attribute__((unused))int client_id,
							char* client_name,\
							__attribute__((unused)) void* context)
{
	printf("client \"%s\" disconnected from overlay\n", client_name);
	return;
}


// imu callback registered to the imu server
static void _imu_helper_cb(__attribute__((unused))int ch, char* data, int bytes,\
											__attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int i, n_packets;
	imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_packets);

	// if there was an error OR no packets received, just return;
	if(data_array == NULL) return;
	if(n_packets<=0) return;

	// time this in debug mode
	int64_t time_before, process_time;
	if(en_debug) time_before = _apps_time_monotonic_ns();

	// flag that imu data is active, skip data if camera is disconnected
	is_imu_connected = 1;
	global_error_codes &= ~ERROR_CODE_IMU_MISSING;
	if(!is_cam_connected) return;


	// add all data into VIO
	for(i=0; i<n_packets; i++){

		// check if we somehow got an out-of-order imu sample and reject it
		if((int64_t)data_array[i].timestamp_ns <= last_imu_timestamp_ns){
			fprintf(stderr, "WARNING got out-of-order imu sample\n");
			continue;
		}

		// pass data straight into mvvislam
		mvVISLAM_AddAccel(mv_vislam_ptr, data_array[i].timestamp_ns, (double)data_array[i].accl_ms2[0],
									(double)data_array[i].accl_ms2[1], (double)data_array[i].accl_ms2[2]);
		mvVISLAM_AddGyro(mv_vislam_ptr, data_array[i].timestamp_ns, (double)data_array[i].gyro_rad[0],
									(double)data_array[i].gyro_rad[1], (double)data_array[i].gyro_rad[2]);

		// record last timestamp so the camera thread is aware
		last_imu_timestamp_ns = data_array[i].timestamp_ns;
	}

	if(en_debug_timing){
		process_time = _apps_time_monotonic_ns() - time_before;
		printf("IMU proc time %6.2fms for %d samples\n", ((double)process_time)/1000000.0, n_packets);
	}

	return;
}


// camera frame callback registered to voxl-camera-server
static void _cam_helper_cb(__attribute__((unused))int ch, camera_image_metadata_t meta,\
							char* frame, __attribute__((unused)) void* context)
{
	/*
	// todo check resolution later after we load lens calibration file
	if(meta.width != cam_cal.width_pixels || meta.height != cam_cal.height_pixels){
		fprintf(stderr, "ERROR camera frame resolution doesn't match calibration resolution\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_RES;
		return;
	}
	else global_error_codes &= ~ERROR_CODE_CAM_BAD_RES;
	*/

	if(meta.format != IMAGE_FORMAT_RAW8){
		fprintf(stderr, "ERROR only support RAW_8 images right now\n");
		global_error_codes |= ERROR_CODE_CAM_BAD_FORMAT;
		return;
	}
	else global_error_codes &= ~ERROR_CODE_CAM_BAD_FORMAT;


	// Make the timestamp be the center of the exposure
	int64_t cam_timestamp_ns = meta.timestamp_ns;
	cam_timestamp_ns += meta.exposure_ns / 2;

	// TODO perhaps just flush the whole buffer instead of only old frames?
	// this keeps the fifo from overflowing
	if(cam_timestamp_ns < (_apps_time_monotonic_ns()-3000000000)){
		fprintf(stderr, "dropping old frame older than 3 seconds\n");
		return;
	}

	// flag that camera data is active, skip frame is imu is disconnected
	is_cam_connected = 1;
	global_error_codes &= ~ERROR_CODE_CAM_MISSING;
	if(!is_imu_connected) return;

	// don't let image go in until IMU has caught up
	// if cam-imu alignment is POSITIVE that means the camera timestamp is early
	// and the image was actually taken after the reported timestamp
	while(last_imu_timestamp_ns < (cam_timestamp_ns + last_time_alignment_ns)){

		// don't get stuck here forever
		if(!main_running) return;
		if(!is_imu_connected) return;
		if(cam_timestamp_ns < (_apps_time_monotonic_ns()-200000000)){
			fprintf(stderr, "ERROR waited more than 0.2 seconds for imu to catch up, dropping frame\n");
			return;
		}
		// small sleep if necessary
		if(en_debug){
			printf("waiting for imu\n");
		}
		usleep(5000);
	}

	// if blank counter is positive then blank out the image
	if(blank_counter>0){
		memset(frame, 128, meta.size_bytes);
		blank_counter--;
		if(en_debug) printf("blanked out image, counter: %d\n", blank_counter);
	}

	// send to mv library, this may take many seconds during reset!!!!
	// time it for now. TODO: logic to handle dropping frames when necessary
	pthread_mutex_lock(&cam_mtx);
	int64_t time_before = _apps_time_monotonic_ns();
	mvVISLAM_AddImage(mv_vislam_ptr, cam_timestamp_ns , (const uint8_t*)frame);
	int64_t process_time = _apps_time_monotonic_ns() - time_before;
	//printf("cam_timestamp_ns: %lld\n", cam_timestamp_ns);

	// record timestamp of camera frame AFTER it's been injested
	last_cam_timestamp_ns = cam_timestamp_ns;
	pthread_mutex_unlock(&cam_mtx);

	if(process_time > 100000000){
		fprintf(stderr, "WARNING slow image proc time: %6.2fms\n", ((double)process_time)/1000000.0);
	}

	if(en_debug_timing){
		printf("CAM proc time %6.2fms\n", ((double)process_time)/1000000.0);
	}

	if(pipe_server_get_num_clients(OVERLAY_CH) > 0){
		pthread_mutex_lock(&draw_mtx);

		//Ensure that we have the correct size array in memory
		if(draw_meta.width * (draw_meta.height - DRAW_BONUS_ROWS_TOP - DRAW_BONUS_ROWS_BOT) != meta.size_bytes){
			if(draw_frame != NULL){
				free(draw_frame);
			}

			draw_meta = meta;
			draw_meta.height += DRAW_BONUS_ROWS_TOP + DRAW_BONUS_ROWS_BOT;
			draw_meta.size_bytes = draw_meta.width * draw_meta.height;

			draw_frame = (uint8_t *)malloc(draw_meta.size_bytes);
		}

		draw_meta = meta;
		draw_meta.height += DRAW_BONUS_ROWS_TOP + DRAW_BONUS_ROWS_BOT;
		draw_meta.size_bytes = draw_meta.width * draw_meta.height;

		memset(draw_frame, 0, draw_meta.width * DRAW_BONUS_ROWS_TOP);
		//memcpy frame to global
		memcpy(draw_frame + (draw_meta.width * DRAW_BONUS_ROWS_TOP), frame, meta.size_bytes);

		memset(draw_frame + (draw_meta.width * DRAW_BONUS_ROWS_TOP) + meta.size_bytes, 0, draw_meta.width * DRAW_BONUS_ROWS_BOT);
		draw_ready = true;
		pthread_mutex_unlock(&draw_mtx);
	}
	return;
}


static void _gps_helper_cb(__attribute__((unused))int ch, char* data, int bytes,\
											__attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	mavlink_message_t* data_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(data_array == NULL) return;

	// no need to do anything unless we have imu/camera data
	if(!is_imu_connected) return;
	if(!is_cam_connected) return;

	// go through every packet in case we got backed up
	for(int i=0; i<n_packets; i++){

		if(data_array[i].msgid != MAVLINK_MSG_ID_VEHICLE_GPS_POSITION){
			if(en_debug){
				fprintf(stderr, "WARNING got GPS message that's not VEHICLE_GPS_POSITION\n");
			}
			break;
		}

		// fetch data out of message directly
		int64_t sys_time_ns = _apps_time_monotonic_ns();
		uint64_t gps_time	= mavlink_msg_vehicle_gps_position_get_time_usec(&data_array[i]);
		int64_t gps_time_ns = gps_time * 1000;
		float vn			= mavlink_msg_vehicle_gps_position_get_vn(&data_array[i]);
		float ve			= mavlink_msg_vehicle_gps_position_get_ve(&data_array[i]);
		float vd			= mavlink_msg_vehicle_gps_position_get_vd(&data_array[i]);
		float vacc			= mavlink_msg_vehicle_gps_position_get_speed_accuracy(&data_array[i]);

		// use PX4's limit of 0.5m/s as the accuracy limit
		if(vacc > 0.5f){
			return;
		}

		// only populate the diagonals for covarience matrix
		// treat speed accuracy as 1 standard deviation, square to get variance
		float64_t measCovVelocity[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
		measCovVelocity[0][0] = vacc * vacc;
		measCovVelocity[1][1] = vacc * vacc;
		measCovVelocity[2][2] = vacc * vacc;

		// send data into vio!
		mvVISLAM_AddGPStimeSync(mv_vislam_ptr,
				sys_time_ns,
				gps_time_ns - sys_time_ns,
				-1);
		mvVISLAM_AddGPSvelocity(mv_vislam_ptr,
				gps_time_ns,
				ve, vn, -vd,
				measCovVelocity, 4);

		if(en_debug){
			printf("new gps data: vn: %6.2f ve: %6.2f vd: %6.2f\n",\
						(double)vn, (double)ve, (double)vd);
		}
	}

	return;
}



// call this instead of return when it's time to exit to cleans up everything
static void _quit(int ret)
{
	pipe_server_close_all();
	pipe_client_close_all();
	mvVISLAM_Deinitialize(mv_vislam_ptr);
	remove_pid_file(PROCESS_NAME);
	if(ret==0) printf("Exiting Cleanly\n");
	exit(ret);
	return;
}


// this is the thread to get data from mvVISLAM and publish to the server pipe
static void* _data_thread_func(__attribute__((unused)) void* context)
{
	int64_t next_time = _apps_time_monotonic_ns() + (int64_t)(1000000000/odr_hz);
	int i,j;
	int64_t time_before, process_time;

	// run until the global main_running flag becomes 0
	while(main_running){

		mvVISLAMPose p;	// pose data from mvvislam
		qvio_data_t d;	// complete "extended" vio MPA packet
		vio_data_t s;	// simplified vio packet
		int nPoints;
		int n_good_points = 0;
		int n_oos_points = 0;

		//make sure we start with a clean data struct and apply any global error codes
		memset(&d,0,sizeof(d));
		d.magic_number = QVIO_MAGIC_NUMBER;
		d.error_code = global_error_codes;
		memset(&s,0,sizeof(s));
		s.magic_number = QVIO_MAGIC_NUMBER;
		s.error_code = global_error_codes;

		// if we are still waiting for imu/camera to start then report it
		if(last_imu_timestamp_ns == 0 || last_cam_timestamp_ns == 0){
			d.state = VIO_STATE_INITIALIZING;
			s.state = VIO_STATE_INITIALIZING;
			d.quality = -1.0f;
			s.quality = -1.0f;
			goto SEND;
		}

		// Grab the pose and feature points
		time_before = _apps_time_monotonic_ns();
		p = mvVISLAM_GetPose(mv_vislam_ptr);
		// nPoints = mvVISLAM_HasUpdatedPointCloud(mv_vislam_ptr);
		process_time = _apps_time_monotonic_ns() - time_before;

		// get features and calculate the number of good features
		#define MAX_POINTS 256
		mvVISLAMMapPoint pPoints[MAX_POINTS];
		nPoints = mvVISLAM_GetPointCloud(mv_vislam_ptr, pPoints,MAX_POINTS);
		for(i=0;i<nPoints;i++){
			if(pPoints[i].pointQuality == 2) n_good_points++;
			if(pPoints[i].pointQuality == 1) n_oos_points++;
		}

		//if(en_debug) printf("total points: %3d  Good Points: %3d\n", nPoints, n_good_points);

		// grab state from mv
		if(p.poseQuality == MV_TRACKING_STATE_FAILED) d.state = VIO_STATE_FAILED;
		else if(p.poseQuality == MV_TRACKING_STATE_INITIALIZING) d.state = VIO_STATE_INITIALIZING;
		else d.state = VIO_STATE_OK;

		// error code is a bitmask that we might have added to already so OR
		// it with VIOs error code
		d.error_code |= p.errorCode;

		// turn off dropped imu error for now, I think this error code is being
		// reported incorrectly by VIO since it's sportadic and seems to have no
		// correlation with IMU rate or phase with camera.
		d.error_code &= ~ERROR_CODE_DROPPED_IMU;

		// populate some other data
		d.quality = _qvio_quality(p);
		d.timestamp_ns = p.time;
		d.imu_cam_time_shift_s = p.timeAlignment;
		last_time_alignment_ns = p.timeAlignment * 1000000000;
		d.n_feature_points = n_good_points;

		// deconstruct mv's 6DRT struct
		for(i=0;i<3;i++){
			d.T_imu_wrt_vio[i] = p.bodyPose.matrix[i][3];
			for(j=0;j<3;j++) d.R_imu_to_vio[i][j] = p.bodyPose.matrix[i][j];
		}
		for(i=0;i<3;i++){
			d.T_ga_vio_wrt_cam[i] = p.gravityCameraPose.matrix[i][3];
			for(j=0;j<3;j++) d.R_ga_vio_to_cam[i][j] = p.gravityCameraPose.matrix[i][j];
		}

		// memcpy the rest
		memcpy(d.pose_covariance,	p.errCovPose,		sizeof(float)*6*6);
		memcpy(d.vel_imu_wrt_vio,	p.velocity,			sizeof(float)*3);
		memcpy(d.vel_covariance,	p.errCovVelocity,	sizeof(float)*3*3);
		memcpy(d.imu_angular_vel,	p.angularVelocity,	sizeof(float)*3);
		memcpy(d.gravity_vector,	p.gravity,			sizeof(float)*3);
		memcpy(d.gravity_covariance,p.errCovGravity,	sizeof(float)*3*3);
		memcpy(d.gyro_bias,			p.wBias,			sizeof(float)*3);
		memcpy(d.accl_bias,			p.aBias,			sizeof(float)*3);
		memcpy(d.R_accl_to_gyro,	p.Rbg,				sizeof(float)*3*3);
		memcpy(d.accl_orthogonality,p.aAccInv,			sizeof(float)*3*3);
		memcpy(d.gyro_orthogonality,p.aGyrInv,			sizeof(float)*3*3);
		memcpy(d.T_cam_wrt_imu,		p.tbc,				sizeof(float)*3);
		memcpy(d.R_cam_to_imu,		p.Rbc,				sizeof(float)*3*3);

		// also make a simplified struct for the common interface
		s.error_code				= d.error_code;
		s.quality					= d.quality;
		s.timestamp_ns				= d.timestamp_ns;
		memcpy(s.T_imu_wrt_vio,		d.T_imu_wrt_vio,	sizeof(float)*3);
		memcpy(s.R_imu_to_vio,		d.R_imu_to_vio,		sizeof(float)*3*3);
		memcpy(s.vel_imu_wrt_vio,	d.vel_imu_wrt_vio,	sizeof(float)*3);
		memcpy(s.imu_angular_vel,	d.imu_angular_vel,	sizeof(float)*3);
		memcpy(s.gravity_vector,	d.gravity_vector,	sizeof(float)*3);
		memcpy(s.T_cam_wrt_imu,		d.T_cam_wrt_imu,	sizeof(float)*3);
		memcpy(s.R_cam_to_imu,		d.R_cam_to_imu,		sizeof(float)*3*3);
		s.n_feature_points			= d.n_feature_points;
		s.state						= d.state;

SEND:
		// send to both pipes
		pipe_server_write(EXTENDED_CH, (char*)&d, sizeof(qvio_data_t));
		pipe_server_write(SIMPLE_CH,   (char*)&s, sizeof(vio_data_t));

		// for debug only
		if(en_debug_timing){
			printf("get pose took %6.2fms", ((double)process_time)/1000000.0);
		}
		if(en_debug){
			printf("state: ");
			pipe_print_vio_state(d.state);
			printf(" err: ");
			pipe_print_vio_error(d.error_code);
			printf("\n");
		}
		if(en_debug_pos){
			printf("%6.3f %6.3f %6.3f ", (double)d.T_imu_wrt_vio[0],(double)d.T_imu_wrt_vio[1],(double)d.T_imu_wrt_vio[2]);
			printf("\n");
		}

		pthread_mutex_lock(&draw_mtx);
		if(draw_ready){

			char output_string[128];
			sprintf(output_string, "Q: %9.5lf XYZ: %6.2lf %6.2lf %6.2lf #Pts: %3d",
								(double)d.quality, (double)d.T_imu_wrt_vio[0], (double)d.T_imu_wrt_vio[1], (double)d.T_imu_wrt_vio[2], n_good_points);
			cCharacter_dwrite_white(draw_frame, draw_meta.width, draw_meta.height, output_string, 5, 9);
			char oos_pts_string[32];
			if(en_debug_overlay) sprintf(oos_pts_string, "#Out of State Pts: %3d", n_oos_points);
			sprintf(output_string, "ex(ms): %6.1f Gain: %5d %s",
								draw_meta.exposure_ns/1000000.0, draw_meta.gain, oos_pts_string);
			cCharacter_dwrite_white(draw_frame, draw_meta.width, draw_meta.height, output_string, 5, (draw_meta.height - DRAW_BONUS_ROWS_BOT) + 9);

			for(i=0;i<nPoints;i++){
				if(pPoints[i].pointQuality == 2){
					cCharacter_draw_plus(draw_frame, draw_meta.width, draw_meta.height, pPoints[i].pixLoc[0], pPoints[i].pixLoc[1] + DRAW_BONUS_ROWS_TOP);
				}else if(en_debug_overlay && pPoints[i].pointQuality == 1){
					cCharacter_draw_dchar(draw_frame, draw_meta.width, draw_meta.height, 'O', pPoints[i].pixLoc[0] - 4, pPoints[i].pixLoc[1] + DRAW_BONUS_ROWS_TOP - 6, 255);
				}
			}

			pipe_server_write_camera_frame(OVERLAY_CH, draw_meta, (char*) draw_frame);

			draw_ready = false;
		}
		pthread_mutex_unlock(&draw_mtx);


		// try to maintain output data rate (odr)
		int64_t current_time = _apps_time_monotonic_ns();
		next_time += (int64_t)(1000000000.0f/odr_hz);
		// uh oh, we fell behind, warn and get back on track
		if(next_time<current_time){
			fprintf(stderr, "WARNING: output data thread fell behind\n");
			next_time =  current_time + (int64_t)(1000000000.0f/odr_hz);
		}
		_nanosleep(next_time-current_time);
	}

	return NULL;
}


// called whenever we connect or reconnect to the imu server
// don't flag as connected yet, not until we get data
static void _imu_connect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	printf("connected to imu server\n");
	pipe_client_set_pipe_size(IMU_CH, IMU_PIPE_MIN_PIPE_SIZE);
	return;
}

// called whenever we disconnect from the imu server
static void _imu_disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "WARNING: disconnected from imu server, resetting VIO\n");
	global_error_codes |= ERROR_CODE_IMU_MISSING;
	last_imu_timestamp_ns = 0;
	is_imu_connected = 0;
	pthread_mutex_lock(&cam_mtx);
	mvVISLAM_Reset(mv_vislam_ptr, 1);
	pthread_mutex_unlock(&cam_mtx);
	return;
}

// called whenever we connect or reconnect to the imu server
// don't flag as connected yet, not until we get data
static void _cam_connect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	printf("connected to camera server\n");
	pipe_client_set_pipe_size(CAM_CH, CAM_PIPE_SIZE);
	return;
}

// called whenever we disconnect from the camera server
static void _cam_disconnect_cb(__attribute__((unused))int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "WARNING: disconnected from camera server, resetting VIO\n");
	global_error_codes |= ERROR_CODE_CAM_MISSING;
	last_cam_timestamp_ns = 0;
	is_cam_connected = 0;
	pthread_mutex_lock(&cam_mtx);
	mvVISLAM_Reset(mv_vislam_ptr, 1);
	pthread_mutex_unlock(&cam_mtx);
	return;
}




// main just reads config files, opens pipes, and waits
int main(int argc, char* argv[])
{
	int i, n;

	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	////////////////////////////////////////////////////////////////////////////////
	// load config
	////////////////////////////////////////////////////////////////////////////////

	// start with the qvio config file as it contains imu/camera names
	printf("loading qvio config file\n");
	if(config_file_read()) return -1;
	// in config only mode, just quit now
	if(en_config_only) return 0;
	// in normal mode print the config for logging purposes
	config_file_print();

	////////////////////////////////////////////////////////////////////////////////
	// gracefully handle an existing instance of the process and associated PID file
	////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

////////////////////////////////////////////////////////////////////////////////
// set this critical process to use FIFO scheduler with high priority
////////////////////////////////////////////////////////////////////////////////

	struct sched_param param;
	memset(&param, 0, sizeof(sched_param));
	param.sched_priority = 95;
	fprintf(stderr, "setting scheduler\n");
	int ret = sched_setscheduler(0, SCHED_FIFO, &param);
	if(ret==-1){
		fprintf(stderr, "WARNING Failed to set priority, errno = %d\n", errno);
		fprintf(stderr, "This seems to be a problem with ADB, the scheduler\n");
		fprintf(stderr, "should work properly when this is a background process\n");
	}
	// check
	ret = sched_getscheduler(0);
	if(ret!=SCHED_FIFO){
		fprintf(stderr, "ERROR failed to set scheduler\n");
	}
	else{
		// even thought this is a success, print to stderr to that it shows up
		// in the correct order. stdout logs in journalctl are usually out of
		// sync with stderr
		fprintf(stderr, "set FIFO priority successfully!\n");
	}

	// The threads created by libmodal_pipe after this should inherit this
	// priority, TODO validate this


////////////////////////////////////////////////////////////////////////////////
// setup
////////////////////////////////////////////////////////////////////////////////

	// starting point for cam alignment comes from config file
	last_time_alignment_ns = cam_imu_timeshift_s * 1000000000;

	// now grab the extrinsic relation between the imu and camera specified in
	// the qvio config file
	printf("loading extrinsics config file\n");
	vcc_extrinsic_t ext[VCC_MAX_EXTRINSICS_IN_CONFIG];
	vcc_extrinsic_t cam_wrt_imu_ext;
	if(vcc_read_extrinsic_conf_file(VCC_EXTRINSICS_PATH, ext, &n, VCC_MAX_EXTRINSICS_IN_CONFIG)) _quit(-1);
	if(vcc_find_extrinsic_in_array(imu_name, cam_name, ext, n, &cam_wrt_imu_ext)){
		fprintf(stderr, "failed to find extrinsics from %s to %s in %s\n", imu_name, cam_name, VCC_EXTRINSICS_PATH);
		_quit(-1);
	}
	vcc_print_extrinsic_conf(&cam_wrt_imu_ext, 1);
	float tbc[3];
	float ombc[3];
	for(i=0;i<3;i++){
		tbc[i] = cam_wrt_imu_ext.T_child_wrt_parent[i];
	}
	_tait_bryan_xyz_intrinsic_to_axis_angle(cam_wrt_imu_ext.RPY_parent_to_child, ombc);


	printf("tbc:  %6.3f %6.3f %6.3f\n", (double)tbc[0], (double)tbc[1], (double)tbc[2]);
	printf("ombc: %6.3f %6.3f %6.3f (axis angle)\n", (double)ombc[0], (double)ombc[1], (double)ombc[2]);
	printf("ombc: %6.3f %6.3f %6.3f (RPY deg)\n", \
							(double)cam_wrt_imu_ext.RPY_parent_to_child[0],\
							(double)cam_wrt_imu_ext.RPY_parent_to_child[1],\
							(double)cam_wrt_imu_ext.RPY_parent_to_child[2]);


	// these are default intrinsic lens constants for ModalAI Tracking camera
	mvCameraConfiguration mv_cam_conf;
	mv_cam_conf.pixelWidth			= 640;
	mv_cam_conf.pixelHeight			= 480;
	mv_cam_conf.memoryStride		= 640;
	mv_cam_conf.uvOffset			= 0;
	mv_cam_conf.principalPoint[0]	= 319.625;
	mv_cam_conf.principalPoint[1]	= 243.144;
	mv_cam_conf.focalLength[0]		= 275.078;
	mv_cam_conf.focalLength[1]		= 274.931;
	mv_cam_conf.distortion[0]		= 0.003908;
	mv_cam_conf.distortion[1]		= -0.009574;
	mv_cam_conf.distortion[2]		= 0.010173;
	mv_cam_conf.distortion[3]		= -0.003329;
	mv_cam_conf.distortion[4]		= 0;
	mv_cam_conf.distortion[5]		= 0;
	mv_cam_conf.distortion[6]		= 0;
	mv_cam_conf.distortion[7]		= 0;
	mv_cam_conf.distortionModel		= 10; // fisheye


	// open opencv calibration file and load instrinsics if present
	std::ifstream fin("/data/modalai/opencv_tracking_intrinsics.yml");
	if(fin.fail()){
		fprintf(stderr, "\nWARNING: Failed to open tracking camera intrinsic file:\n");
		fprintf(stderr, "/data/modalai/opencv_tracking_intrinsics.yml\n");
		fprintf(stderr, "using default tracking camera intrinsics for now\n");
		fprintf(stderr, "follow instructions here to calibrate the tracking cam:\n");
		fprintf(stderr, "https://docs.modalai.com/calibrate-cameras/\n\n");
	}
	else{
		printf("loading /data/modalai/opencv_tracking_intrinsics.yml\n");

		int hasM = 0;
		int hasD = 0;
		YAML::Node doc = YAML::Load(fin);
		if(doc==NULL){
			fprintf(stderr, "failed to parse tracking camera intrinsics file\n");
		}
		else{
			if(doc["M"]["data"]){
				hasM = 1;
				mv_cam_conf.focalLength[0]		= doc["M"]["data"][0].as<double>();
				mv_cam_conf.focalLength[1]		= doc["M"]["data"][4].as<double>();
				mv_cam_conf.principalPoint[0]	= doc["M"]["data"][2].as<double>();
				mv_cam_conf.principalPoint[1]	= doc["M"]["data"][5].as<double>();
			}
			if(doc["D"]["data"]){
				hasD = 1;
				// TODO get distortion model here too, for now we only use 10 (fisheye)
				mv_cam_conf.distortion[0]	= doc["D"]["data"][0].as<double>();
				mv_cam_conf.distortion[1]	= doc["D"]["data"][1].as<double>();
				mv_cam_conf.distortion[2]	= doc["D"]["data"][2].as<double>();
				mv_cam_conf.distortion[3]	= doc["D"]["data"][3].as<double>();
			}
		}

		if(!hasM || !hasD){
			fprintf(stderr, "Failed to find both M and D entries in /data/modalai/opencv_tracking_intrinsics.yml\n");
		}

		fin.close();
	}

	printf("using camera intrinsics:\n");
	printf("focal lengths: %f %f\n", mv_cam_conf.focalLength[0], mv_cam_conf.focalLength[1]);
	printf("principle points: %f %f\n", mv_cam_conf.principalPoint[0], mv_cam_conf.principalPoint[1]);
	printf("distortion: %f %f %f %f\n", mv_cam_conf.distortion[0],mv_cam_conf.distortion[1],mv_cam_conf.distortion[2],mv_cam_conf.distortion[3]);

	// static parameters
	float tba[3] = {0, 0, 0}; // position of gps relative to imu
	char* staticMaskFileName = NULL; // for image mask
	float logDepthBootstrap = log(1.0); // unused depth bootstrap

	// pass it all in
	mv_vislam_ptr = mvVISLAM_Initialize(
		&mv_cam_conf,
		cam_imu_timeshift_s,
		tbc,
		ombc,
		cam_imu_timeshift_s,
		T_cam_wrt_imu_uncertainty,
		R_cam_to_imu_uncertainty,
		cam_imu_timeshift_s_uncertainty,
		accl_fsr_ms2,
		gyro_fsr_rad,
		accl_noise_std_dev,
		gyro_noise_std_dev,
		cam_noise_std_dev,
		min_std_pixel_noise,
		fail_high_pixel_noise_points,
		logDepthBootstrap,
		use_camera_height_bootstrap,
		log(camera_height_off_ground_m),
		!enable_init_while_moving,
		limited_imu_bw_trigger,
		staticMaskFileName,
		gps_imu_time_alignment_s,
		tba,
		enable_mapping);

	if(mv_vislam_ptr == NULL){
		fprintf(stderr, "Error creating mvVISLAM object\n");
		_quit(-1);
	}

	printf("Please ignore the error about Configuration.SF.xml above. ^^^\n");
	printf("It's an optional file, and should be a warning not an error\n");


////////////////////////////////////////////////////////////////////////////////
// start the server pipe and data thread to write to it
////////////////////////////////////////////////////////////////////////////////

	int flags = SERVER_FLAG_EN_CONTROL_PIPE;

	// init extended pipe
	pipe_info_t info1 = { \
		QVIO_EXTENDED_NAME,			// name
		QVIO_EXTENDED_LOCATION,		// location
		"qvio_data_t",				// type
		PROCESS_NAME,				// server_name
		QVIO_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(EXTENDED_CH, info1, flags)){
		_quit(-1);
	}

	// add in optional fields to the info JSON file
	cJSON* json = pipe_server_get_info_json_ptr(EXTENDED_CH);
	cJSON_AddStringToObject(json, "imu", imu_pipe_location);
	cJSON_AddStringToObject(json, "cam", cam_pipe_location);
	pipe_server_update_info(EXTENDED_CH);
	pipe_server_set_available_control_commands(EXTENDED_CH, CONTROL_COMMANDS);


	// init simple pipe
	pipe_info_t info2 = { \
		QVIO_SIMPLE_NAME,			// name
		QVIO_SIMPLE_LOCATION,		// location
		"vio_data_t",				// type
		PROCESS_NAME,				// server_name
		VIO_RECOMMENDED_PIPE_SIZE,	// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(SIMPLE_CH, info2, flags)){
		_quit(-1);
	}

	// add in optional fields to the info JSON file
	json = pipe_server_get_info_json_ptr(SIMPLE_CH);
	cJSON_AddStringToObject(json, "imu", imu_pipe_location);
	cJSON_AddStringToObject(json, "cam", cam_pipe_location);
	pipe_server_update_info(SIMPLE_CH);
	pipe_server_set_available_control_commands(SIMPLE_CH, CONTROL_COMMANDS);


	// init overlay pipe
	pipe_info_t info3 = { \
		QVIO_OVERLAY_NAME,			// name
		QVIO_OVERLAY_LOCATION,		// location
		"camera_image_metadata_t",		// type
		PROCESS_NAME,				// server_name
		CAM_PIPE_SIZE,				// size_bytes
		0							// server_pid
	};

	if(pipe_server_create(OVERLAY_CH, info3, flags)){
		_quit(-1);
	}

	pipe_server_set_connect_cb(OVERLAY_CH, _overlay_connect_cb, NULL);
	pipe_server_set_disconnect_cb(OVERLAY_CH, _overlay_disconnect_cb, NULL);
	pipe_server_set_available_control_commands(OVERLAY_CH, CONTROL_COMMANDS);


	// indicate to the soon-to-be-started thread that we are initialized
	// and running, this is an extern variable in start_stop.c
	main_running = 1;

	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	pthread_create(&data_thread, &tattr, _data_thread_func, NULL);

////////////////////////////////////////////////////////////////////////////////
// now subscribe to cam and imu servers, waiting if they are not alive yet
// VIO needs IMU before camera so init in that order.
////////////////////////////////////////////////////////////////////////////////

	// try to open a pipe to the imu server
	pipe_client_set_connect_cb(IMU_CH, _imu_connect_cb, NULL);
	pipe_client_set_disconnect_cb(IMU_CH, _imu_disconnect_cb, NULL);
	pipe_client_set_simple_helper_cb(IMU_CH, _imu_helper_cb, NULL);
	printf("waiting for imu\n");
	ret = pipe_client_open(IMU_CH, imu_pipe_location, PROCESS_NAME, \
			EN_PIPE_CLIENT_SIMPLE_HELPER, \
			IMU_RECOMMENDED_READ_BUF_SIZE);
	// check for error
	if(ret<0){
		fprintf(stderr, "Failed to open IMU pipe\n");
		pipe_print_error(ret);
		_quit(-1);
	}

	// try to open pipe to camera server
	pipe_client_set_connect_cb(CAM_CH, _cam_connect_cb, NULL);
	pipe_client_set_disconnect_cb(CAM_CH, _cam_disconnect_cb, NULL);
	pipe_client_set_camera_helper_cb(CAM_CH, _cam_helper_cb, NULL);
	printf("waiting for cam\n");
	ret = pipe_client_open(CAM_CH, cam_pipe_location, PROCESS_NAME, \
				EN_PIPE_CLIENT_CAMERA_HELPER, 0);
	// check for error
	if(ret<0){
		fprintf(stderr, "Failed to open CAM pipe\n");
		pipe_print_error(ret);
		_quit(-1);
	}

	// open optional GPS pipe
	if(enable_gps_vel){
		pipe_client_set_simple_helper_cb(GPS_CH, _gps_helper_cb, NULL);
		ret = pipe_client_open(GPS_CH, GPS_PIPE_NAME, PROCESS_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER, \
				MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	}


	// run until start/stop module catches a signal and changes main_running to 0
	while(main_running) usleep(5000000);


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	pthread_join(data_thread, NULL);
	_quit(0);
	return 0;
}
